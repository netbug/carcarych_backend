<html lang="en">


<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>CarCarych, we care about your health</title>

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <!--  Scripts-->
    <script src="js/jquery-3.2.0.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.charts.load("visualization", "1", {packages:["corechart"]});
        var timerId = setInterval(function(){
            $.ajax({
                type: "GET",
                url: "http://carcarych.azurewebsites.net/AzureApp/api",
                contentType: "text/plain",
                dataType: "text",
                success: function (data) {
                    var intArray = data.substring(1,data.length-1).split(",").map(Number);
                    var tableBlinks = new google.visualization.DataTable();
                    tableBlinks.addColumn('number', 'Time');
                    tableBlinks.addColumn('number', 'Value');
                    $.each(intArray.slice(0,data.length<60?data.length-1:59), function (index, value) {
                        tableBlinks.addRow([index, value]);
                    });
                    var optionsBlinks = {
                        title: 'Registered  Blinks:',
                        width: '100%', height: '100%',
                        legend: 'none',
                        curveType: 'function',
                        titleTextStyle: {
                            color: '#ff9800',
                            fontName: 'sans-serif',
                            fontSize: 18,
                            bold: true,
                            italic: false
                        },
                        vAxis: {
                            title: 'Blinks'
                        },
                        hAxis: {
                            title: 'Seconds'
                        }
                    };
                    var tableFreq = new google.visualization.DataTable();
                    tableFreq.addColumn('number', 'Time');
                    tableFreq.addColumn('number', 'Value');
                    for(var i = 60;i<=intArray.length;i+=60){
                        var sum = 0;
                        for (var k = i-60; k<i; k++)
                            sum+= intArray[k];
                        tableFreq.addRow([i/60,sum]);
                    }
                    var optionsFreq = {
                        title: 'Blinking Frequency:',
                        width: '100%', height: '100%',
                        legend: 'none',
                        curveType: 'function',
                        titleTextStyle: {
                            color: '#ff9800',
                            fontName: 'sans-serif',
                            fontSize: 18,
                            bold: true,
                            italic: false
                        },
                        vAxis: {
                            title: 'Blinks'
                        },
                        hAxis: {
                            title: 'Minutes'
                        }
                    };
                    $(".chart").empty();
                    var chartBlinks = new google.visualization.LineChart(document.getElementById('chart_blinks'));
                    chartBlinks.draw(tableBlinks, optionsBlinks);
                    var chartFreq = new google.visualization.LineChart(document.getElementById('chart_freq'));
                    chartFreq.draw(tableFreq,optionsFreq);
                }
            });},2000);

    </script>
</head>
<body>
<nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Abrakadabra Team</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="index.html">Back</a></li>
        </ul>

        <ul id="nav-mobile" class="side-nav">
            <li><a href="index.html">Back</a></li>
        </ul>
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">
            <div class="col s6 m12 l6">
                <div  id="chart_blinks" class="chart">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col s6 m12 l6">
                <div  id="chart_freq" class="chart">
                    <div class="preloader-wrapper big active" >
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                            <div class="circle"></div>
                        </div><div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="section">
        <div class="row">
            <div class="col s6 m12 l6">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <span class="card-title">Driving statistics</span>
                        <p>Today you have been driving for 1 hour 22 minutes.
                            We recommend to have a rest at least every 2 hours.</p>
                    </div>
                </div>
            </div>
            <div class="col s6 m12 l6">
                <div class="card blue-grey darken-1">
                    <div class="card-content white-text">
                        <span class="card-title">Our recommendations</span>
                        <p>Today you have been sleeping for 5 hours.
                            It is less than usual, we recommend you to be more careful on road. </p>
                    </div>
                </div>
            </div>
    </div>
    <br><br>

    <div class="section">


    </div>
</div>


</div>
<footer class="page-footer orange">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Team Abrakadabra</h5>
                <p class="grey-text text-lighten-4">We are a bunch of students working on this project and we like good coffee.</p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
        </div>
    </div>
</footer>



</body>
</html>
