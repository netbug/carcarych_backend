package com.unn.azureapp;

import com.unn.datamodel.DataStorage;

import javax.xml.bind.JAXBException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by feeldruss on 22.03.17.
 */
public abstract class ListUpdateObject {
    static private Date date = null;
    static public  void put(String data) throws JAXBException {
        List<Integer> list = new ArrayList<Integer>();
        String[] numbers = data.substring(1,data.length()-1).split(",");
        for (String num: numbers) {
            list.add(Integer.decode(num));
        }
        DataStorage dataStorage = DataAccessObject.getList();
        List<Integer> dataList = dataStorage.getList();
        dataList.addAll(0,list);
        if(dataList.size()>300) dataList = dataList.subList(0,299);
        dataStorage.setList(dataList);
        DataAccessObject.pushList(dataStorage);
        date = new Date();
    }

    static public String get() throws JAXBException {
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        DataStorage dataStorage = DataAccessObject.getList();
        List<Integer> list = dataStorage.getList();
        for (Integer integer: list) {
            builder.append(integer).append(',');
        }
        builder.deleteCharAt(builder.length()-1).append(']');
        return builder.toString();
    }

    static public boolean online() {
        return date != null && new Date().getTime() - date.getTime() < 120000;
    }
}
